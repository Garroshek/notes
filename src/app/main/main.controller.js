(function() {
  'use strict';

  angular
    .module('notes')
    .controller('MainController', MainController)
    .controller('AppController', AppController)

  /** @ngInject */
  function AppController ($scope, FileUploader) {
    $scope.uploader = new FileUploader();
    $scope.uploader.url = 'http://localhost:8080/restserver/resources/files/upload';
    $scope.uploader.queueLimit = 1;

  }

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, notesList, $scope, $http, $window, $translate) {
    var vm = this;

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1467973141058;
    vm.showToastr = showToastr;
    vm.loadedNotes = [];
    vm.getOneNote = getOneNote;
    vm.addNote = addNote;
    vm.deleteNote = deleteNote;
    vm.updateNote = updateNote;

    activate();

    $scope.changeLanguage = function (langKey) {
      $translate.use(langKey);
    };

    function activate() {
      getWebDevTec();
      getListNotes();
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }

    function getListNotes() {
      vm.loadedNotes = notesList.query(function getNotes() {
        console.log(vm.loadedNotes);
      });
    }

    function getOneNote(id) {
      vm.loadedNote = notesList.get({id: id}, function getNote() {
        console.log(vm.loadedNote);
      });
    }

    function addNote() {
      $http({ url: 'http://localhost:8080/restserver/resources/notes/',
        method: 'POST',
        data: {title: $scope.noteTitle, body: $scope.noteBody}
      }).then(function successCallback(response) {
        $scope.noteTitle = null;
        $scope.noteBody = null;
        getListNotes();
      }, function errorCallback(response) {
      });
    }

    function deleteNote(id){
        $http({ url: 'http://localhost:8080/restserver/resources/notes/' + id,
          method: 'DELETE',
          data: {id: id},
          headers: {"Content-Type": "application/json;charset=utf-8"}
        }).then(function successCallback(response) {
          $window.location.reload();
        }, function errorCallback(response) {
        });
    }

    function updateNote(id) {
      $http({ url: 'http://localhost:8080/restserver/resources/notes/' + id,
        method: 'PUT',
        data: {title: $scope.editTitle, body: $scope.editBody}
      }).then(function successCallback(response) {
        $window.location.reload();
      }, function errorCallback(response) {
      });
    }
  }
})();
