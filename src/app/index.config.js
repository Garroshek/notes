(function() {
  'use strict';

  angular
    .module('notes')
    .config(config)
    .config(translate);

  /** @ngInject */
  function config($logProvider, toastrConfig) {
    // Enable log
    $logProvider.debugEnabled(true);
    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
  }

  /** @ngIngect */
  function translate($translateProvider){
    var translationsEN = {
      HEADLINE: 'My notes',
      MOAR: 'More info',
      EDIT: 'Edit note',
      EDITFORM: 'Edit form of note',
      NOTETITLE: 'Note title',
      NOTEBODY: 'Note body',
      SAVECHANGES: 'Save changes',
      DELETENOTE: 'Delete note',
      ADDNOTE: 'Add note',
      ADDMANYNOTES: 'Add list notes',
      ERRORWORD: 'Error!',
      ERRORPHRASE: 'Length of note don\'t must be more 250 characters',
      UPLOADLISTNOTES: 'Upload list notes',
      CHOOSEFILE: 'Choose file',
      FILENAME: 'Name of file'
    }
    var translationsRU = {
      HEADLINE: 'Заметки',
      MOAR: 'Подробнее',
      EDIT: 'Изменить заметку',
      EDITFORM: 'Форма редактирования заметки',
      NOTETITLE: 'Заголовок заметки',
      NOTEBODY: 'Содержимое заметки',
      SAVECHANGES: 'Сохранить изменения',
      DELETENOTE: 'Удалить заметку',
      ADDNOTE: 'Добавить заметку',
      ADDMANYNOTES: 'Добавить список заметок',
      ERRORWORD: 'Ошибка!',
      ERRORPHRASE: 'Длина записки не должна превышать 250 символов',
      UPLOADLISTNOTES: 'Загрузить cписок заметок',
      CHOOSEFILE: 'Выберите файл',
      FILENAME: 'Имя файла'
    }
    $translateProvider.translations('en', translationsEN);
    $translateProvider.translations('ru', translationsRU);
    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.forceAsyncReload(true);
  }
})();
