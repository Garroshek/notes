(function() {
  'use strict';

  angular
    .module('notes', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize',
      'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap',
      'toastr', 'pascalprecht.translate', 'ngSanitize', 'angularFileUpload']);
})();
