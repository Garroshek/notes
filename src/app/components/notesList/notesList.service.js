(function() {
  'use strict';

  angular
    .module('notes')
    .factory('notesList', notesList);

  /** @ngInject */
  function notesList($resource) {
    return $resource('http://localhost:8080/restserver/resources/notes/:id');
  }
})();
